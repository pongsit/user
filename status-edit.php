<?php
	
require_once("../system/init.php");

$role = new \pongsit\role\role();
$user = new \pongsit\user\user();

if(empty($_GET['id'])){
	echo 'User id is required!';
	exit();
}
$id = +$_GET['id'];

if($role->get_max_power($_SESSION['user']['id']) <= $role->get_max_power($id)){
	echo 'คุณไม่มีสิทธิ์ครับ';
	exit();
}

$infos = $user->get_info($id);
// error_log(print_r($infos,true));
if(!$infos['active']){
	$user->update_status($id,1);
}else{
	$user->update_status($id,0);
}

// reverse back if id is 1
if($id == 1){
	$user->update_status($id,1);
}

$infos = $user->get_info($id);
if($infos['active']==1){
	echo 1;
}else{
	echo '-1';
}