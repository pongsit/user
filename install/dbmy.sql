CREATE TABLE `user` (
  `id` INT AUTO_INCREMENT primary key NOT NULL,
  `name` text NOT NULL,
  `password` text NOT NULL,
  `active` int(11) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO user (name,password) VALUES ('Admin','$2y$04$w1.5N9S7G9/TA40LK2SGzeA3Od5fA.FM1D8GU2ViGfh9aOghg3yE.');