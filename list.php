<?php
	
require_once("../system/init.php");

$option = new \pongsit\option\option();
$role = new \pongsit\role\role();
$user = new \pongsit\user\user();

if(!($role->check('admin') || $role->check('staff') || $role->check('manager')  || $_SESSION['user']['id']==1)){
	$view = new \pongsit\view\view('locked');
	echo $view->create();
	exit();
}

$role_id = '';
if(!empty($_GET['role_id'])){
	$role_id = $_GET['role_id'];
	$max_power = $role->get_max_power($_SESSION['user']['id']);
	$role_power = $role->get_power($role_id);
	if($role_power > $max_power){
		$view = new \pongsit\view\view('locked');
		echo $view->create();
		exit();
	}
}

// ---- ajax
$variables=array();
// $variables['h1'] = $view->block('h1',array('message'=>'รายชื่อผู้ใช้','css'=>'col-12 text-center'));
$variables['sort']='desc';
if(!empty($_GET['sort'])){
	if($_GET['sort']=='desc'){
		$variables['sort']='asc';
	}else{
		$variables['sort']='desc';
	}
}

$variables['order_by']='id';
if(!empty($_GET['order_by'])){
	$variables['order_by']=$_GET['order_by'];
}

if(empty($role_id)){
	$variables['list'] = $view->ajax('show-more-div',array(
	'url'=>'ajax.php',
	'block'=>'list',
	'json'=>'{"limit":"20","offset":"0","order_by":"'.$variables['order_by'].'","sort":"'.$variables['sort'].'"}'
	));
}else{
	$variables['list'] = $view->ajax('show-more-div',array(
	'url'=>'ajax.php',
	'block'=>'list',
	'json'=>'{"type":"show-more-div-filter","limit":"20","offset":"0","role_id":"'.$role_id.'","sort":"'.$variables['sort'].'"}'
	));
}
$variables['search-div'] = $view->ajax('search-div',array(
'url'=>'ajax.php',
'block'=>'list',
'json'=>'{"limit":"20","offset":"0","order_by":"'.$variables['order_by'].'","sort":"'.$variables['sort'].'"}'
));

// ----
$view = new \pongsit\view\view('list');
$view->create();
if(empty($role_id)){
	$n = $user->count_active();
}else{
	$n = $user->role_count_active($role_id);
}
// $variables['subtitle'] = 'ทั้งหมด '.$n.' คน';
$variables['subtitle'] = '';
$variables['user-count'] = number_format($n);
// $variables['user-count'] = number_format('10000');
$variables['page-name'] = 'รายชื่อผู้ใช้';
echo $view->create($variables);
