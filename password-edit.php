<?php

require_once("../system/init.php");

$option = new \pongsit\option\option();
$role = new \pongsit\role\role();
$user = new \pongsit\user\user();

if(empty(+$_GET['id'])){
	$view = new \pongsit\view\view('warning');
	echo $view->create($variables);
	exit();
}else{
	$id = +$_GET['id'];
}

if(!($_SESSION['user']['id']==$id || $_SESSION['user']['id']==1 || $role->check('admin') || $role->check('manager'))){
	$view = new \pongsit\view\view('locked');
	echo $view->create();
	exit();
}

$variables=array();
$variables['notification']='';
$variables['page-name'] = 'แก้รหัส';

if(!empty($_POST)){
	if(!empty($_POST['new_password']) && !empty($_POST['new_password_repeat'])){
		if($_POST['new_password'] == $_POST['new_password_repeat']){
			$user->update_password($id,$auth->generatePassword($_POST['new_password']));
			header('Location:'.$path_to_core.'user/edit.php?id='.$id.'&notification=password-changed');
			exit();
		}else{
			$variables['notification']=$view->block('alert',array('type'=>'danger','message'=>'Password 2 ครั้ง ไม่เหมือนกันครับ','css'=>'col-md-8'));
		}
	}else{
		$variables['notification']=$view->block('alert',array('type'=>'danger','message'=>'Password ไม่สามารถปล่อยว่างได้ครับ','css'=>'col-md-8'));
	}
}
$user_infos = $user->get_info($id);
$variables['this_user_id'] = $id;
$variables['username_now'] = $user_infos['name'];
echo $view->create($variables);
