<?php
	
require_once("../system/init.php");

$option = new \pongsit\option\option();
$role = new \pongsit\role\role();
$user = new \pongsit\user\user();

if(empty(+$_GET['id'])){
	$view = new \pongsit\view\view('warning');
	echo $view->create($variables);
	exit();
}else{
	$id = +$_GET['id'];
}

if(!($_SESSION['user']['id']==$id || $_SESSION['user']['id']==1 || $role->check('admin') || $role->check('manager'))){
	$view = new \pongsit\view\view('locked');
	echo $view->create();
	exit();
}

$user_infos = $user->get_info($id);

// add view
$variables=array();
$variables['notification']='';
$variables['username_now']='<a href="'.$path_to_core.'user/edit.php?id='.$id.'">'.$user_infos['name'].'</a>';
$variables['page-name']='แก้ชื่อผู้ใช้';
if(!empty($_POST['name'])){
	if($user->update_name($id,$_POST['name'])){
		header('Location: '.$path_to_core.'user/edit.php?id='.$id.'&notification=username-changed');
	}else{
		$variables['notification']=$view->block('alert',array('type'=>'danger','css'=>'col-md-7','message'=>'Username ซ้ำครับ'));
	}
}
echo $view->create($variables);
