<?php

namespace pongsit\user;

class user extends \pongsit\model\model{
	
	public function __construct(){
		parent::__construct();
    }
    
	function get_id($username){
		$rows = $this->db->query_array0("SELECT id FROM ".$this->table." WHERE name = '".$username."';");
		return $rows['id'];
	}

	function update_password($user_id,$password){
		$this->db->update('user',array('password'=>$password),'id='.$user_id);
	}
	
	function update_name($user_id,$name){
		$id_from_username = $this->get_id($name);
		if(empty($id_from_username)){
			$this->db->update('user',array('name'=>$name),'id='.$user_id);
			return 1;
		}else{
			return 0;
		}
	}
	
	function update_status($user_id,$new_status){
		$this->db->update('user',array('active'=>$new_status),'id='.$user_id);
	}
	
	function get_all_user(){
		$results = $this->db->query_array("select * from ".$this->table.";");
		return $results;
	}
	
	function exist($username){
		$results = $this->db->query_array("SELECT id FROM ".$this->table." WHERE name = '".$username."';");
		if(!empty($results[0]['id'])){
			return true;
		}else{
			return false;
		}
	}
	function insert_role($user_id,$role_id){
		return $this->db->insert('role_user',array('user_id'=>$user_id,'role_id'=>$role_id));
	}
	function delete_role($user_id){
		return $this->db->delete('role_user','user_id='.$user_id);
	}
	function delete_line($user_id){
		return $this->db->delete('user_line','user_id='.$user_id);
	}
	
	function role_get_all_active($inputs=array()){
		$role_id='role_id';
		if(!empty($inputs['role_id'])){ $role_id=$inputs['role_id']; }
		$order_by='user.id';
		if(!empty($inputs['order_by'])){ $order_by=$inputs['order_by']; }
		$sort='DESC';
		if(!empty($inputs['sort'])){ $sort=$inputs['sort']; }
		$limit='';
		if(!empty($inputs['limit'])){ $limit='LIMIT '.$inputs['limit']; }
		$offset='';
		if(!empty($inputs['offset'])){ $offset='OFFSET '.$inputs['offset']; }
		$query = "SELECT *, user_id as id FROM `".$this->table."` inner join role_user on user.id=role_user.user_id 
					where user.active=1 and role_user.role_id='$role_id' ORDER BY $order_by $sort $limit $offset;";
		$outputs = $this->db->query_array($query);
		return $outputs;
	}
	
	function role_count_active($role_id){
		$query = "SELECT count(*) as c FROM `".$this->table."` inner join role_user on user.id=role_user.user_id 
					where user.active=1 and role_user.role_id='$role_id';";
		$outputs = $this->db->query_array0($query);
		return $outputs['c'];
	}

// 	function get_all_in_the_team_active($inputs=array()){
// 		if(empty($inputs['team_id'])){ return; }
// 		$team_id = $inputs['team_id'];
// 		$order_by='id';
// 		if(!empty($inputs['order_by'])){ $order_by=$inputs['order_by']; }
// 		$sort='DESC';
// 		if(!empty($inputs['sort'])){ $sort=$inputs['sort']; }
// 		$limit='';
// 		if(!empty($inputs['limit'])){ $limit='LIMIT '.$inputs['limit']; }
// 		$offset='';
// 		if(!empty($inputs['offset'])){ $offset='OFFSET '.$inputs['offset']; }
// 		$query = "SELECT *, user.id as id, team.name as team_name, user.name as name FROM ".$this->table." 
// 			  INNER JOIN user_team ON user.id = user_team.user_id
// 			  INNER JOIN team ON user_team.team_id = team.id 
// 				   WHERE user_team.team_id = '".$inputs['team_id']."' 
// 				   	 AND team.active = 1 
// 				   	 AND user.active = 1 
// 				   ORDER BY $order_by $sort $limit $offset;";
// 		// error_log($query);
// 		$outputs = $this->db->query_array($query);
// 		return $outputs;
// 	}
// 	function quick_search_team($inputs=array()){
// 		if(empty($inputs['team_id'])){ return; }
// 		$team_id = $inputs['team_id'];
// 		$order_by='user.id';
// 		if(!empty($inputs['order_by'])){ $order_by=$inputs['order_by']; }
// 		$sort='DESC';
// 		if(!empty($inputs['sort'])){ $sort=$inputs['sort']; }
// 		$limit='';
// 		if(!empty($inputs['limit'])){ $limit='LIMIT '.$inputs['limit']; }
// 		$offset='';
// 		if(!empty($inputs['offset'])){ $offset='OFFSET '.$inputs['offset']; }
// 		$query = "SELECT * FROM ".$this->table." 
// 			  INNER JOIN user_team ON user.id = user_team.user_id
// 				   WHERE ".$this->table.".name like '%".$inputs['q']."%' 
// 				   	 AND user_team.team_id = '".$inputs['team_id']."' 
// 				   ORDER BY $order_by $sort $limit $offset;";
// 		error_log($query);
// 		$outputs = $this->db->query_array($query);
// 		return $outputs;
// 	}
}