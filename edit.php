<?php

require_once("../system/init.php");

if(empty(+$_GET['id'])){
	$view = new \pongsit\view\view('warning');
	echo $view->create($variables);
	exit();
}else{
	$id = +$_GET['id'];
	$user_id = $id;
}

$role = new \pongsit\role\role();
$user = new \pongsit\user\user();
$profile = new \pongsit\profile\profile();
$line = new \pongsit\line\line();

// ผู้ที่เข้าชมได้
if(!($role->check('admin') || $role->check('manager') || $role->check('staff') || $id==$_SESSION['user']['id'])){
	$view = new \pongsit\view\view('locked');
	echo $view->create();
	exit();
}

// ผู้ที่แก้ไขได้
$editable = false;
if($role->get_max_power($_SESSION['user']['id']) > $role->get_max_power($id)){
	$editable = true;
}

$notification = '';
if(!empty($_GET['notification'])){
	switch($_GET['notification']){
		case 'password-changed': $notification = 'เปลี่ยนรหัสผ่านเรียบร้อย'; break;
		case 'role-changed': $notification = 'เปลี่ยนบทบาทเรียบร้อย'; break;
		case 'username-changed': $notification = 'เปลี่ยน Username เรียบร้อย'; break;
		case 'profile-edited': $notification = 'เปลี่ยนข้อมูลเรียบร้อย'; break;
	}
}

$user_infos = $user->get_info($id);
// print_r($user_infos);
$profile_infos = $profile->get_all_infos($id);

if(empty($user_infos)){
	$view = new \pongsit\view\view('message');
	$variables = array();
	$variables['message'] = '<div class="yellow">ไม่มีข้อมูลครับ</div>';
	echo $view->create($variables);
	exit();
}

$user_info = '';
$variables=array();
$variables['label']='Username:';
if($_SESSION['user']['id']==$id || $editable){
	$variables['info']='<a href="'.$path_to_core.'user/username-edit.php?id='.$id.'">'.$user_infos['name'].'</a>';
}else{
	$variables['info']=$user_infos['name'];
}
$user_info .= $view->block('info-list-row',$variables);

$variables=array();
$variables['label']='Password:';
if($_SESSION['user']['id']==$id || $editable){
	$variables['info']='<a href="'.$path_to_core.'user/password-edit.php?id='.$id.'">เปลี่ยน</a>';
}else{
	$variables['info']='****';
}
$user_info .= $view->block('info-list-row',$variables);

$variables=array();
$variables['label']='Line:';
$line_infos = $line->get_info_from_user_id($id);
if(!empty($line_infos)){
	if(!empty($line_infos['displayName'])){
		$variables['info']=$line_infos['displayName'];
	}else{
		$variables['info']='ผูกเรียบร้อย';
	}
	$user_info .= $view->block('info-list-row',$variables);
}else{
	// $variables['info']='<a href="'.$path_to_core.'line-link.php?id='.$id.'">เชื่อมต่อ</a>';
}

$variables=array();
$role_shows['label']='Role:';
$role_show = '';
$comma = '';
$role_names = $role->get_all_name_show_for($id);

if(!empty($role_names)){
	foreach($role_names as $value){
		$role_show .= $comma.$value;
		$comma = ', ';
	}
}else{
	$role_shows['label']='';
}
$role_shows['info']=$role_show;

$comma = '';

if($_SESSION['user']['id']==1 || $editable){
	if(empty($role_show)){
		$role_shows['label']='Role';
		$role_show = 'เพิ่มบทบาท';
	}
	// $role_shows['info']=$role_show;
	$role_shows['info']='<a href="'.$path_to_core.'role/user-edit.php?id='.$id.'">'.$role_show.'</a>';
}

$user_info .= $view->block('info-list-row',$role_shows);

if($_SESSION['user']['id']==1 || $editable){
	$status_show = $view->ajax('toggle',array(
		'url'=>'status-edit.php?id='.$id,
		'active_message'=>'Active',
		'inactive_message'=>'Inactive',
		'css'=>'',
		'default_status'=>$user_infos['active']
	));
	$user_info .=$view->block('info-status',array('status'=>$status_show));
}

$profile_alls = $profile->get_all_column_active(array('order_by'=>'weight','sort'=>'asc'));

// $skips = array('title','first_name','last_name');
$skips = array();
$last_id=-1;
$user_profile='';
if(!empty($profile_alls)){
	foreach($profile_alls as $values){
		if(in_array($values['name'],$skips)){continue;}
		$profile_values = $profile->get_value_filter($values['id'],$id);
		$multiple_count=1;
		$multiple_show='';
		foreach($profile_values as $_values){
			if($last_id==$values['id']){$multiple_count++;}
			if($multiple_count>1){$multiple_show=$multiple_count;}
			$_variables['label']=$values['name_show'].$multiple_show.':';
			$_variables['info']=$_values['value'];
			$variables['profile'][$values['name']] = $_values['value'];
			if(empty(trim($_values['value']))){continue;}
			$user_profile .= $view->block('info-list-row',$_variables);
			$last_id = $values['id'];
		}
	}
}

// $variables['h1'] = $view->block('h1',array('message'=>'ข้อมูลที่เกี่ยวข้อง','css'=>'col-md-7 text-center'));
$variables['user_id_from_get'] = $id;
$variables['user-info']=$user_info;
$variables['edit-button']='';
if(!empty($user_profile)){
	if($_SESSION['user']['id']==$id || $editable){
		$variables['edit-button']='<a href="'.$path_to_core.'profile/edit.php?filter_id='.$id.'">แก้ไข</a>';
	}else{
		$variables['edit-button']='';
	}
	$variables['user-profile']=$user_profile;
}else{ 
	$variables['user-profile']='<div class="col-12 text-center"><a href="'.$path_to_core.'profile/edit.php?filter_id='.$id.'">กรุณาเพิ่มข้อมูล</a></div>';
}
if(!empty($notification)){
	$variables['notification']=$view->block('alert',array('type'=>'success','css'=>'col-12','message'=>$notification));
}else{
	$variables['notification']=$notification;
}
// $variables['profile-image'] = $view->ajax('upload-image',array(
// 'url'=>$path_to_core.'profile-ajax.php',
// 'css'=>'hide',
// 'json'=>'{type:"image-upload",path:"'.$path_to_root.'app/img/profile/",user_id:"'.$id.'"}',
// 'call_back'=>'location.reload();'
// ));

$variables['this_user_id'] = +$_GET['id'];
$variables['page-name'] = 'แก้ไขข้อมูล';


$variables['profile_image']=$path_to_core.'system/img/profile/male-avatar.png';
if(file_exists($path_to_root.'app/system/img/profile/'.$id)){
	$variables['profile_image']=$path_to_root.'app/system/img/profile/'.$id;
}

$variables['upload-image'] = $view->ajax('upload-image',array(
'url'=>'ajax.php',
'css'=>'btn btn-outline-primary btn-sm',
'json'=>'{user_id:'.$user_id.'}',
'call_back' => 'window.location = "'.$path_to_core.'croppie/edit.php?id='.$user_id.'"'
));

$variables['user_id'] = $user_id;
$variables['image_version'] = rand(1000,9999);

if(empty($skip_echo)){ // ให้ค่า $skip_echo เมื่อต้องการเพิ่มข้อมูลต่อท่ายก่อนแสดงผล
	echo $view->create($variables);
}
