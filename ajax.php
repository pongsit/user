<?php
	
require_once("../system/init.php");
$user = new \pongsit\user\user();
$role = new \pongsit\role\role();

if(empty($_POST['type'])){
	exit();
}

switch($_POST['type']){
	case 'quick-search-id':
	case 'quick-search-fill': 
	case 'search-div': 
		$results = $user->quick_search(@$_POST['q']);
		break;
	case 'show-more': 
		$results = $user->get_all_active($_POST);
		break;
	case 'show-more-div-filter': 
		$results = $user->role_get_all_active($_POST);
		break;
	case 'image-upload':
		$file = new \pongsit\file\file();

		// error_log(print_r($_POST,true));
		// error_log(print_r($_FILES,true));

		/*
		$_POST = array(
			[path] => ../system/img/profile/
			[user_id] => 1
		)

		$_FILES = Array
		(
		    [file] => Array
		        (
		            [name] => Screen Shot 2563-08-20 at 23.58.43.png
		            [type] => image/png
		            [tmp_name] => /Applications/MAMP/tmp/php/php7ICYwh
		            [error] => 0
		            [size] => 14670
		        )

		)


		*/

		$results = array();
		if(empty($_FILES)){
			$results['error'] = 'Error: No file found!';
		}else{
			$file_type = $_FILES['file']['type'];

			$ext = '';
			if($file_type == 'image/jpeg' || $file_type == 'image/png'){
				if(!empty($_POST['user_id'])){
					$user_id = $_POST['user_id'];
				}else{
					$results['error'] = 'Error: No user_id';
					exit();
				}
				$path_infos = pathinfo($file_type);
				// $ext = $path_infos['basename'];
				switch($file_type){
					case 'image/jpeg': $ext='jpg'; break;
					case 'image/png': $ext='png'; break;
				}
				if(!file_exists($path_to_app.'system/img/profile/')){
					mkdir($path_to_app.'system/img/profile/',0755,true);
				}
				$file->delete_all_with_file_name($path_to_app.'system/img/profile/'.$user_id);
				rename($_FILES['file']['tmp_name'], $path_to_app.'system/img/profile/'.$user_id);
				chmod($path_to_app.'system/img/profile/'.$user_id,0644);
				$results['path'] = $path_to_app.'system/img/profile/'.$user_id;
			}else{
				$results['error'] = '.jpg or .png file only!';
			}
		}
		break;
}

// error_log('Hi '.print_r($results,true));

if(empty($results)){
	exit();
}

// foreach($results as $key=>$values){
// 	unset($results[$key]['password']);
// 	$results[$key]['name']='<a href="'.$path_to_core.'user-info.php?id='.$results[$key]['id'].'">'.$results[$key]['name'].'</a>';
// 	if($results[$key]['status']==1){
// 		$results[$key]['status']='<span style="color:green">Active</span>';
// 	}else{
// 		$results[$key]['status']='<span style="color:red">Inactive</span>';
// 	}
// 	$all_role = implode(', ',$role->get_all($results[$key]['id']));
// 	$results[$key]['role']=$all_role;
// }

header('Content-Type: application/json');
echo json_encode($results);