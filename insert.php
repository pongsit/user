<?php
	
require_once("../system/init.php");

$option = new \pongsit\option\option();
$role = new \pongsit\role\role();
$user = new \pongsit\user\user();

if(!($role->check('admin') || $role->check('manager') || $_SESSION['user']['id'] == 1)){
	$view = new \pongsit\view\view('locked');
	echo $view->create();
	exit();
}

$notification = '';
if(!empty($_POST)){
	if( !empty($_POST['username']) && 
		!empty($_POST['password']) &&
		!empty($_POST['password_retype'])
	){
		unset($_POST['submit']);
		if($_POST['password']==$_POST['password_retype']){
			if(!$user->exist($_POST['username'])){
				$hash = $auth->generatePassword($_POST['password']);
				
				$latest_id = $user->insert(array('name'=>$_POST['username'],'password'=>$hash));
				// $user_id = $user->get_last_inserted_id();
				// $user->insert($user_id);
				// $notification = $view->block('alert',array('message'=>'เพิ่มผู้ใช้เรียบร้อย','type'=>'success','css'=>'col-7'));
				header('Location: '.$path_to_core.'user/edit.php?id='.$latest_id);
				exit();
			}else{
				$notification = $view->block('alert',array('message'=>'มี username นี้อยู่ในระบบแล้วครับ','type'=>'danger','css'=>'col-7'));
			}
		}else{
			$notification = $view->block('alert',array('message'=>'Password 2 ครั้งไม่ตรงกันครับ','type'=>'danger','css'=>'col-7'));
		}
		
	}else{
		if(!empty($_SESSION['team']['id'])){
			$notification = $view->block('alert',array('message'=>'กรุณากรอกข้อมูลที่มีเครื่องหมาย * ให้ครบทุกช่องครับ','type'=>'danger','css'=>'col-7'));
		}else{
			$notification = $view->block('alert',array('message'=>'ไม่สามารถเพิ่ม User ได้เนื่องจากขาดข้อมูลทีมครับ','type'=>'danger','css'=>'col-7'));
		}
	}
}

$variables['notification'] = $notification;
$variables['h1']=$view->block('h1',array('message'=>'แบบฟอร์มเพิ่มผู้ใช้ระบบ','css'=>'col-md-7 text-center'));
// $role_ids = array();
// if(!empty($_SESSION['user']['id'])){
// 	$role_ids = $role->get_all_role_id($_SESSION['user']['id'],$_SESSION['team']['id']);
// }
// 
// $max_role = $role_ids[0]['role_id'];
// $skips = array();
// for($i=$max_role-1;$i>0;$i--){
// 	$skips[$i] = $i;
// }
// $variables['role_option'] = $option->from_db('role','last',$skips);
$view = new \pongsit\view\view('insert');
echo $view->create($variables);

