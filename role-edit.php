<?php
	
require_once("../system/init.php");

$option = new pongsit\option\option();
$role = new pongsit\role\role();
$user = new pongsit\user\user();


if(empty(+$_GET['id'])){
	$view = new \pongsit\view\view('warning');
	echo $view->create($variables);
	exit();
}else{
	$id = +$_GET['id'];
}

if(!($_SESSION['user']['id']==1 || $role->check('admin') || $role->check('manager') || $role->check('staff'))){
	$view = new \pongsit\view\view('locked');
	echo $view->create();
	exit();
}

if(!empty($_POST)){
		unset($_POST['submit']);
		$user->delete_role($id);
		foreach($_POST['check'] as $value){
			$_role_id = $role->get_id($value);
			if(!in_array($_role_id,$aboves)){
				$user->insert_role($id,$_role_id);
			}
		}
		header('Location:'.$path_to_core.'user/edit.php?id='.$id.'&notification=role-changed');
		exit();
}

$using_power = $role->get_max_power($_SESSION['user']['id']);
$this_power = $role->get_max_power($id);
if($using_power < $this_power){
	$view = new \pongsit\view\view('locked');
	echo $view->create();
	exit();
}

// $variables['h1'] = $view->block('h1',array('message'=>'แบบฟอร์มเปลี่ยนบทบาท','css'=>'col-12 text-center'));
$role_option = '';
$role_ids = $role->get_all_name_for($id);
$role_options = $option->checkbox($table='role',$checks=$role_ids,'where active=1 order by power desc');
foreach($role_options as $key=>$value){
	if($using_power >= $role->get_power($key)){
		$role_option .= $value.' ';
	}
}
$user_infos = $user->get_info($id);
$variables['this_user_id'] = $id;
$variables['username_now']='<a href="'.$path_to_core.'user/edit.php?id='.$id.'">'.$user_infos['name'].'</a>';
$variables['role-option'] = $role_option;
$variables['toggle-1'] = 'hide';
$variables['toggle-2'] = '';
$variables['page-name'] = 'เปลี่ยนบทบาท';

echo $view->create($variables);
