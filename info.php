<?php

// ตัวอย่างเรียกหน้านี้ไปใช้งานที่อื่น
// require_once __DIR__ . '/../../vendor/pongsit/system/init.php';
// require_once __DIR__ . '/../../vendor/pongsit/user/info.php';
// echo $view->create($variables);

$incs = get_included_files(); 
foreach($incs as $k=>$v){
	if($k != 0){
		if (strpos($v, 'pongsit/user') !== false) {
		    $skip_echo = true;
		}
	}
}

if(empty($skip_echo)){
	require_once("../system/init.php");
}

$role = new \pongsit\role\role();
$user = new \pongsit\user\user();
$profile= new \pongsit\profile\profile();
$line = new \pongsit\line\line();
$linenotify = new \pongsit\linenotify\linenotify();

if(empty(+$_GET['id'])){
	$view = new \pongsit\view\view('warning');
	echo $view->create($variables);
	exit();
}else{
	$id = +$_GET['id'];
	$user_id = +$_GET['id'];
}

$is_staff = 0;
if($role->check('admin') || $role->check('manager') || $role->check('staff')){
	$is_staff = 1;
}

if(!($id==$_SESSION['user']['id'] || $is_staff)){
	$view = new \pongsit\view\view('locked');
	echo $view->create();
	exit();
}

$editable = false;
if($_SESSION['user']['id']==1 || $id==$_SESSION['user']['id']){
	$editable = true;
}

$notification = '';
if(!empty($_GET['notification'])){
	switch($_GET['notification']){
		case 'password-changed': $notification = 'เปลี่ยนรหัสผ่านเรียบร้อย'; break;
		case 'role-changed': $notification = 'เปลี่ยนบทบาทเรียบร้อย'; break;
		case 'username-changed': $notification = 'เปลี่ยน Username เรียบร้อย'; break;
		case 'profile-edited': $notification = 'เปลี่ยนข้อมูลเรียบร้อย'; break;
	}
}

$user_infos = $user->get_info($id);
$profile_infos = $profile->get_all_infos($id);

if(empty($user_infos)){
	$view = new \pongsit\view\view('message');
	$variables = array();
	$variables['message'] = '<div class="yellow">ไม่มีข้อมูลครับ</div>';
	echo $view->create($variables);
	exit();
}

$variables['this_username'] = '<div class="overflow-hidden">'.$user_infos['name'].'</div>';

if(!empty($notification)){
	$variables['notification']='
<div class="row">
	<div class="col-12 col-md-9">
		<div class="alert alert-success alert-dismissible fade show" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
			'.$notification.'
		</div>
	</div>
</div>';
}else{
	$variables['notification']=$notification;
}

$variables['photo']=$path_to_core.'system/img/profile/male-avatar.png';
if(file_exists($path_to_root.'app/system/img/profile/'.$id)){
	$variables['photo']=$path_to_root.'app/system/img/profile/'.$id;
}

$variables['this_user_id'] = +$_GET['id'];
$variables['page-name'] = 'ข้อมูลที่เกี่ยวข้อง';
$variables['first_name'] = '';
if(!empty($profile_infos['first_name'])){
	$variables['first_name'] = '<span class="text-nowrap">'.$profile_infos['first_name'].'</span>';
}
$variables['last_name'] = '';
if(!empty($profile_infos['last_name'])){
	$variables['last_name'] = '<span class="text-nowrap">'.$profile_infos['last_name'].'</span>';
}
$variables['nickname'] = '';
if(!empty($profile_infos['nickname'])){
	$variables['nickname'] = '<span class="text-nowrap">'.$profile_infos['nickname'].'</span>';
}
$variables['profile_name'] = '';
if(!empty($variables['first_name']) || !empty($variables['last_name']) || !empty($variables['nickname'])){
	$nickname = '';
	if(!empty($variables['nickname'])){
		$nickname = ' ('.$variables['nickname'].')';
	}
	$variables['profile_name'] = $variables['first_name'].' '.$variables['last_name'].$nickname.'<br>';
}
$variables['id'] = $id;
$variables['line_at'] = '';
if(!empty($confVariables['line']['at'])){
	$variables['line_at'] = 'รับข้อมูลใหม่ก่อนใครได้ที่นี่ครับ 
			<a href="https://line.me/R/ti/p/'.$confVariables['line']['at'].'">
				<img class="rounded" height="36" border="0" alt="เพิ่มเพื่อน" src="'.$path_to_core.'system/img/icon/line.png">
			</a>';
}
$_check = '';
$line_infos = $line->get_info_from_user_id($id);
if(!empty($line_infos['userId'])){
	if(!empty($linenotify->get_access_token($line_infos['userId']))){
		$_check = 'checked';
	}
}

$variables['linenotify'] = '';
if(!empty($confVariables['linenotify']['client_id'])){
	if($_SESSION['user']['id']==$id){
		$variables['linenotify'] = '
		การแจ้งเตือน <label id="linenotify-toggle" class="switch">
		  <input type="checkbox" '.$_check.'>
		  <span class="slider round"></span>
		</label>
		<script>
		$(function(){
			var is_check = $("#linenotify-toggle").find("input").is(":checked");
			$("body").on("click tap","#linenotify-toggle",function(){
				if(!is_check && $(this).find("input").is(":checked")){
					location.href="'.$path_to_core.'linenotify/register.php";
				}
				if(is_check && !($(this).find("input").is(":checked"))){
					var $post = $.post( "'.$path_to_core.'linenotify/ajax.php", { "type":"off" } );
					$post.done(function(data){
						if(data.done){
							window.location.replace("'.$current_url.'");
						}
					});
					is_check = false;
				}
			});
		});
		</script>
		';
	}else{
		$variables['linenotify'] = '
			การแจ้งเตือน <label id="linenotify-toggle" class="switch not-active">
			  <input type="checkbox" disabled '.$_check.'>
			  <span class="slider round" disabled></span>
			</label>
			';
	}
}

$variables['is_staff'] = $is_staff;

if(empty($skip_echo)){ 
	echo $view->create($variables);
}
